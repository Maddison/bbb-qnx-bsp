/*
 * I2C LCD test program to Display hostname and IP address
 *
 * LCD data:  MIDAS  MCCOG22005A6W-FPTLWI  Alphanumeric LCD, 20 x 2, Black on White, 3V to 5V, I2C, English, Japanese, Transflective
 * LCD data sheet, see:
 *                      http://au.element14.com/midas/mccog22005a6w-fptlwi/lcd-alpha-num-20-x-2-white/dp/2425758?ost=2425758&selectedCategoryId=&categoryNameResp=All&searchView=table&iscrfnonsku=false
 *
 *  BBB P9 connections:
 *    - P9_Pin17 - SCL - I2C1	 GPIO3_2
 *    - P9_Pin18 - SDA - I2C1    GPIO3_1
 *
 *  LCD connections:
 *    - pin 1 � VOUT to the 5V     (external supply should be used)
 *    - pin 4 � VDD  to the 5V
 *    - pin 8 � RST  to the 5V
 *
 *    - pin 2 - Not connected  (If 3.3V is used then add two caps)
 *    - pin 3 - Not connected  (If 3.3V is used then add two caps)
 *
 *    - pin 5 - VSS  to Ground
 *    - pin 6 � SDA  to the I2C SDA Pin
 *    - pin 7 - SCL  to the I2C SCL Pin
 *
 * Author:  Samuel Ippolito
 * Date:	16/05/2017
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <net/if.h>
//#include <netinet/tcp.h>

#include <fcntl.h>
#include <devctl.h>
#include <hw/i2c.h>
#include <errno.h>
#include <unistd.h>
#include <sys/neutrino.h>


#define DATA_SEND 0x40  // sets the Rs value high
#define Co_Ctrl   0x00  // mode to tell LCD we are sending a single command


// Function prototypes
int main(int argc, char *argv[]);
int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData);
void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column);
void Initialise_LCD(int fd, _Uint32t LCDi2cAdd);
void readIPaddress(char * IPaddress);

void readIPaddress(char * IPaddress)
{
	char *command = "ifconfig dm0";
	FILE *fpipe;
	char line[256] = {};
	char *e=NULL;
	char *p=NULL;

	fpipe = (FILE*)popen(command,"r");
	if( fpipe != NULL )
	{
	 while( fgets(line, sizeof(line), fpipe) )
	 {
		 // Find the line with 'inet ' in it
		 //printf("%s",line);
         if (p = strstr(line, "inet "))
         {
    		 //printf("%s",line);
    		 //printf("%c",*p);
        	 p = strchr(p, ' ');  	// find the position of the first space after 'inet'
        	 e = &IPaddress[0];		// set the buffer pp
        	 p++;
    		 while( (*p) != '\0') 	// pointer arithmetic, iterates through memory
    		 {
    			 //printf("%c",*p);
    			 if((*p)!=' ')  	// need to stop at the first white space
    			 {
    				 *e = *p;
    				 e++;
    				 p++;
    			 }
    			 else break;		// don't copy the rest of the string
    		 }
    		 *e++ = '\0';  // make sure there is a null terminator at the end of the IPaddress
    		 //printf("%s",IPaddress);
         }
	 }
	 pclose(fpipe);
	}
	else printf("Error, problems with pipe to ifconfig dm0!\n");

	/*
	// get ip address of node

	int s;
	struct ifreq ifreq;
	char dst[INET_ADDRSTRLEN];
	struct sockaddr_in *sa_in;


	s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s == -1)
	{
		perror("socket");
		return EXIT_FAILURE;
	}

	memset(&ifreq, 0x00, sizeof(ifreq));

	strcpy(ifreq.ifr_name, "en0");

	if (ioctl(s, SIOCGIFADDR, &ifreq) == -1)
	{
		perror("ioctl");
		return EXIT_FAILURE;
	}

	sa_in = (struct sockaddr_in *)&ifreq.ifr_addr;
	inet_ntop(AF_INET, &sa_in->sin_addr.s_addr, dst, sizeof(dst));
	printf("en0: %s\n", dst);
	*/

}

int main(int argc, char *argv[])
{
	int file;
	int error;
	volatile uint8_t LCDi2cAdd = 0x3C;
	_Uint32t speed = 10000; // nice and slow (will work with 200000)
	uint8_t	LCDdata[21] = {};


	// Open I2C resource and set it up
	if ((file = open("/dev/i2c1",O_RDWR)) < 0)	  // OPEN I2C1
	{
		printf("Error while opening Device File.!!\n");
		exit(EXIT_FAILURE);
	}
	else
		//printf("I2C1 Opened Successfully\n");

	error = devctl(file,DCMD_I2C_SET_BUS_SPEED,&(speed),sizeof(speed),NULL);  // Set Bus speed
	if (error)
	{
		fprintf(stderr, "Error setting the bus speed: %d\n",strerror ( error ));
		exit(EXIT_FAILURE);
	}
	else
		//printf("Bus speed set = %d\n", speed);

	Initialise_LCD(file, LCDi2cAdd);

	usleep(1);

	// write hostname to LCD screen at position 0,0
	char hostnm[250] = {};   // #define BUF_SIZE 256
	memset(hostnm, '\0', 250);
	hostnm[250 - 1] = '\n';
	gethostname(hostnm, sizeof(hostnm));
	//printf("  --> Machine hostname is: '%s'\n", hostnm);
	SetCursor(file, LCDi2cAdd,0,0); // set cursor on LCD to first position first line
	I2cWrite_(file, LCDi2cAdd, DATA_SEND, &hostnm[0], 20);		// write new data to I2C


	char * BadIP = "0.0.0.0";
//	char * BadIP = "192.168.0.12";  // for debug only
	char IPaddress[256] = {};
	readIPaddress(IPaddress);
	//printf("%s",IPaddress);
	if(strcmp(IPaddress,BadIP))
	{
		// Got valid IP address, so we can display it and exit
		SetCursor(file, LCDi2cAdd,1,0); // set cursor on LCD to first position first line
		I2cWrite_(file, LCDi2cAdd, DATA_SEND, &IPaddress[0], 20);		// write new data to I2C
		printf("Node hostname: %s\n",hostnm);
		printf("Node IP address is: %s\n",IPaddress);
		pclose(file);
		return EXIT_SUCCESS;
	}
	else
	{
		// no IP yet, so we need to wait for DHCP server
		printf("Node hostname: %s\n",hostnm);
		printf("Waiting for IP address from DHCP server....\n");
		int i;
		for(i=120;i>=0;i--)
		{
			// check if IP has been assigned
			readIPaddress(IPaddress);
			if(strcmp(IPaddress,BadIP))
			{
				// Got valid IP address, so we can display it and exit
				SetCursor(file, LCDi2cAdd,1,0); // set cursor on LCD to first position first line
				I2cWrite_(file, LCDi2cAdd, DATA_SEND, &IPaddress[0], 20);		// write new data to I2C
				printf("Node hostname: %s\n",hostnm);
				printf("Node IP address is: %s\n",IPaddress);
				pclose(file);
				return EXIT_SUCCESS;
			}

			sprintf(LCDdata,"waiting DHCP: %d",i);
			SetCursor(file, LCDi2cAdd,1,0); // set cursor on LCD to first position first line
			I2cWrite_(file, LCDi2cAdd, DATA_SEND, &LCDdata[0], 20);		// write new data to I2C
			//printf("Debug: %s",IPaddress);
			sleep(1);
		}
	}

	printf("Failed to get IP address from DHCP server: %s\n",IPaddress);
	sprintf(LCDdata,"Error: DHCP Failed !");
	SetCursor(file, LCDi2cAdd,1,0); // set cursor on LCD to first position first line
	I2cWrite_(file, LCDi2cAdd, DATA_SEND, &LCDdata[0], 20);		// write new data to I2C
	pclose(file);
	//printf("\n complete");
	return EXIT_SUCCESS;
}


// Writes to I2C
int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData)
{
	i2c_send_t hdr;
    iov_t sv[2];
    int status, i;

    uint8_t LCDpacket[21] = {};  // limited to 21 characters  (1 control bit + 20 bytes)

    // set the mode for the write (control or data)
    LCDpacket[0] = mode;  // set the mode (data or control)

	// copy data to send to send buffer (after the mode bit)
	for (i=0;i<NbData+1;i++)
		LCDpacket[i+1] = *pBuffer++;

    hdr.slave.addr = Address;
    hdr.slave.fmt = I2C_ADDRFMT_7BIT;
    hdr.len = NbData + 1;  // 1 extra for control (mode) bit
    hdr.stop = 1;

    SETIOV(&sv[0], &hdr, sizeof(hdr));
    SETIOV(&sv[1], &LCDpacket[0], NbData + 1); // 1 extra for control (mode) bit
      // int devctlv(int filedes, int dcmd,     int sparts, int rparts, const iov_t *sv, const iov_t *rv, int *dev_info_ptr);
    status = devctlv(fd, 		  DCMD_I2C_SEND, 2,          0,          sv,              NULL,           NULL);

    if (status != EOK)
    	printf("status = %s\n", strerror ( status ));

    return status;
}


void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column)
{
	uint8_t position = 0x80; // SET_DDRAM_CMD (control bit)
	uint8_t rowValue = 0;
	uint8_t	LCDcontrol = 0;
	if (row == 1)
		rowValue = 0x40;     // memory location offset for row 1
	position = (uint8_t)(position + rowValue + column);
	LCDcontrol = position;
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}


void Initialise_LCD (int fd, _Uint32t LCDi2cAdd)
{
	uint8_t	LCDcontrol = 0x00;

	//   Initialise the LCD display via the I2C bus
	LCDcontrol = 0x38;  // data byte for FUNC_SET_TBL1
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x39; // data byte for FUNC_SET_TBL2
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x14; // data byte for Internal OSC frequency
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x79; // data byte for contrast setting
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x50; // data byte for Power/ICON control Contrast set
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x6C; // data byte for Follower control
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x0C; // data byte for Display ON
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x01; // data byte for Clear display
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}

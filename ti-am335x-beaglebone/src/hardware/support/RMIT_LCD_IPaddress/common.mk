ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)
include $(MKFILES_ROOT)/qmacros.mk

LIBS+=drvr 

NAME =RMIT_LCD_IPaddress

EXTRA_SILENT_VARIANTS+=$(SECTION)
USEFILE=$(PROJECT_ROOT)/$(NAME).use

define PINFO
PINFO DESCRIPTION=Writes hostname of node and IP address to LCD module connected to I2C1
endef

#The following line must appear because it causes bsp_tag.sh to insert
#INSTALL_ROOT_nto defines after it and hence before qtargets.
#Those defines must appear before qtargets to ensure that the vpath includes
#the staging area created by bsp_validate.
-include $(PROJECT_ROOT)/roots.mk


#####AUTO-GENERATED by packaging script... do not checkin#####
   INSTALL_ROOT_nto = $(PROJECT_ROOT)/../../../../install
   USE_INSTALL_ROOT=1
##############################################################

include $(MKFILES_ROOT)/qtargets.mk



#calibrator version
VERSION=1.4

#defines
#for QNX the wlan_cu file name is different
if [ -e  wlan_cu_ti18xx ]; then
    WLAN_CU='./wlan_cu_ti18xx -b -m' #run the wlan_cu: bypass the supplicant (-b) and mute mode (-m)
    TMP_FILE='/tmp/tmp.txt'
else
	WLAN_CU='./wlan_cu -b -m' #run the wlan_cu: bypass the supplicant (-b) and mute mode (-m)
    TMP_FILE='tmp.txt'
fi

INI_FILE_NAME='./tiwlan.ini'
MENU_PWR_MODE='/ t '
MENU_TX_CMD='/ t t '
MENU_RX_CMD='/ t r '
MENU_CONFIG_CMD='/ t c '

#function for printing version
function print_version {
	echo "calibrator version "$VERSION
}

#function for printing set_mac command help
function print_set_mac_help {
	echo "Usage:  calibrator [options] dev <devname> plt set_mac <tiwlan.ini file> [<MAC address>|from_fuse|default]"
	echo ""
	echo "Set a MAC address to the NVS file."
	echo ""
	echo "<MAC address>   specific address to use (XX:XX:XX:XX:XX:XX)"
	echo "from_fuse       try to read from the fuse ROM, if not available the command fails"
	echo "default         write 00:00:00:00:00:00 to have the driver read from the fuse ROM,"
	echo "                fails if not available"
}
						
#function for printing help
function print_help {
	echo "Usage:  calibrator [dev] <devname> <section> command <parameters>"
	echo "Options:"
	echo "      --version       show version"
	echo "      --help          show help"
			   
	echo "      dev <devname> plt power_mode <on|off|fem_detect|chip_awake>"
	echo "      dev <devname> plt set_mac <tiwlan.ini file> [<MAC address>|from_fuse|default]"
	echo "      dev <devname> plt get_mac"
	echo "      dev <devname> wl18xx_plt stop_tx" 
	echo "      dev <devname> wl18xx_plt start_tx <delay> <rate> <size> <mode> <data_type> <gi> <options1> <options2> <source MAC> <dest MAC> <channel-width>"	
	echo "      dev <devname> wl18xx_plt tx_tone"
	echo "      dev <devname> wl18xx_plt get_rx_stats"
	echo "      dev <devname> wl18xx_plt stop_rx"
	echo "      dev <devname> wl18xx_plt start_rx <source address> <destination address>"		
	echo "      dev <devname> wl18xx_plt tune_channel <channel> <band> <bandwidth>"
	echo "      dev <devname> wl18xx_plt set_tx_power <output_power> <level> <band> <channel_number> <primary_channel_location> <antenna> <non_serving_channel> <channel_limitation> <internal> <gain_calculation_mode> <analog_gain_control_id> <post_dpd_gain>"
	echo "      dev <devname> wl18xx_plt set_antenna_mode_24G <mac_prim_rx_chain> <mac_prim_tx_chain> <mac_rx_chain1_en> <mac_rx_chain2_en> <mac_tx_chain1_en> <mac_tx_chain2_en>"
	echo "      dev <devname> wl18xx_plt set_antenna_mode_5G <mac_prim_rx_chain> <mac_rx_chain1_en> <mac_rx_chain2_en> <mac_tx_chain1_en>"
	echo "      dev <devname> wl18xx_plt phy_reg_read <addr>"
	echo "      dev <devname> wl18xx_plt phy_reg_write <addr> <data>"
	echo "      dev <devname> wl18xx_plt tx_tone_start <mode> <bin index> <antenna mode> <gain index>"
	echo "      dev <devname> wl18xx_plt tx_tone_stop"
	echo "      dev <devname> wl18xx_plt set_antenna_diversity_5G <mode>"
}

#function for executing a command
function execute_command {
	echo $* >> $TMP_FILE
	$WLAN_CU < $TMP_FILE
	rm $TMP_FILE
}

#function for updating the ini file
function change_val_ini_file {
	grep "$1 *=" $INI_FILE_NAME > /dev/null
	if [ $? == 0 ]; then
		sed -i "s/$1.*/$1 = $2/" $INI_FILE_NAME
	else
		echo >> $INI_FILE_NAME
		echo "$1 = $2" >> $INI_FILE_NAME
	fi
}

#check if no option were entered or user wants to see help menu
if [ $# -eq 0 ] || [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
	print_help
	exit 0
fi

#check if user wants to see the version
if [ "$1" == "--version" ] || [ "$1" == "-v" ]; then
	print_version
	exit 0
fi

#check if user entered "dev" as first parameter and update the flags accordingly
if [ "$1" == "dev" ]; then
	INTERFACE="$2"
	SECTION="$3"
	COMMAND="$4"
	INI_FILE="$5"
	MAC_ADDR="$6"
	MAC_ADDR_ID=6
	PARAMS_ID=4	
else
	INTERFACE="$1"
	SECTION="$2"
	COMMAND="$3"
	INI_FILE="$4"
	MAC_ADDR="$5"
	MAC_ADDR_ID=5
	PARAMS_ID=3	
fi

#show help if not enough parameters
if [ $# -lt $PARAMS_ID ]; then
	print_help
	exit 0
fi

#validate the structure
if [ $INTERFACE != "wlan0" ] && [ $INTERFACE != "tiw_sta0" ]; then
	print_help
	exit 0
elif [ $SECTION != "plt" ] && [ $SECTION != "wl18xx_plt" ]; then
	print_help
	exit 0
fi

#handle the set_mac command (this command is not sent to the wlan_cu but handled by this script)
if [ $COMMAND == "set_mac" ]; then
	if [ $# -lt $MAC_ADDR_ID ]; then
		print_set_mac_help
		exit 0
	elif [ $INI_FILE == "tiwlan.ini" ]; then
		if [ $MAC_ADDR == "from_fuse" ] || [ $MAC_ADDR == "default" ] || [ $MAC_ADDR == "00:00:00:00:00:00" ]; then
			change_val_ini_file "MacAddressOverride" "00 00 00 00 00 00"
			echo "MAC Address set OK. You must reload the driver for the changes to take effect."
			exit 0
		else	
			if [ ! -z "$(echo "$MAC_ADDR" | sed -n '/^\([0-9A-F][0-9A-F]:\)\{5\}[0-9A-F][0-9A-F]$/p')" ]; then
				MAC_ADDR=$(echo $MAC_ADDR|sed 's/:/ /g')
				change_val_ini_file "MacAddressOverride" "$MAC_ADDR"
				echo "MAC Address set OK. You must reload the driver for the changes to take effect."
				exit 0;
			else
				echo "Invalid MAC address format or wrong option. No changes were made."
				exit 0
			fi
		fi
	else
		print_set_mac_help
		exit 0
	fi	
fi

#prepare the argument list
INDEX=1
for TOKEN in $*
do
	if [ $INDEX -ge $PARAMS_ID ]; then
		COMMAND_SUFFIX=$COMMAND_SUFFIX" "$TOKEN
	fi
	((INDEX+=1)) 
done
	
#find the required command
if [ $COMMAND == "power_mode" ]; then
	COMMNAD_PREFIX=$MENU_PWR_MODE
	COMMAND_SUFFIX=$(echo $COMMAND_SUFFIX|sed 's/on/0/g')
	COMMAND_SUFFIX=$(echo $COMMAND_SUFFIX|sed 's/off/1/g')
	COMMAND_SUFFIX=$(echo $COMMAND_SUFFIX|sed 's/fem_detect/2/g')
	COMMAND_SUFFIX=$(echo $COMMAND_SUFFIX|sed 's/chip_awake/3/g')
elif [ $COMMAND == "stop_tx" ] || [ $COMMAND == "start_tx" ] || [ $COMMAND == "tx_tone_start" ] || [ $COMMAND == "tx_tone_stop" ]; then
	COMMNAD_PREFIX=$MENU_TX_CMD
elif [ $COMMAND == "start_rx" ] || [ $COMMAND == "stop_rx" ] || [ $COMMAND == "get_rx_stats" ]; then 
	COMMNAD_PREFIX=$MENU_RX_CMD
else
	COMMNAD_PREFIX=$MENU_CONFIG_CMD
fi	

#build the command
COMMNAD_TO_SEND=$COMMNAD_PREFIX$COMMAND_SUFFIX

#execute the command
execute_command $COMMNAD_TO_SEND


